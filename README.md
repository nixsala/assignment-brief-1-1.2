import java.util.Scanner;

public class PrimeNumber {

    public static void main(String[] args) {

    	Scanner scan = new Scanner (System.in);
    	System.out.println("Enter the number : ");
    	double num= scan.nextDouble();

        boolean expression = false;
        for(int i = 2; i <= num/2; i=i+1)
        {
            // condition for nonprime number
            if(num % i == 0)
            {
               expression = true;
                break;
            }
       
        }
    	
    	
        if (!expression)
            System.out.println(num + " is a prime number.");
        else
            System.out.println(num + " is not a prime number.");
       
    }
    
    	
    }


    
